package webrr

import (
	"errors"
	"testing"
)

type Writer interface {
	Write([]byte) (int, error)
}

type writer string

func (w *writer) Write(bts []byte) (int, error) {
	*w = writer(string(bts))
	return 0, nil
}

func TestToJSON(t *testing.T) {
	tables := []struct {
		in  string
		out string
	}{
		{"", "{\"error\":\"\""},
		{"msg", "{\"error\":\"msg\""},
		{"message", "{\"error\":\"message\""},
		{"big messageeeeeeeeeeeee", "{\"error\":\"big messageeeeeeeeeeeee\""},
		{"!@#$%^&*()_+", "{\"error\":\"!@#$%^&*()_+\""},
		{"1238351564646546546568465dsdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd", "{\"error\":\"1238351564646546546568465dsdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd\""},
	}

	for _, table := range tables {
		if str := toJSON(table.in); str != table.out {
			t.Errorf("FAIL: want \"%s\", got \"%s\"", table.out, str)
		}
	}
}

func TestAdd(t *testing.T) {
	Strings := []string{"", "dcsdcds", "ssddddddddd", "sda"}
	for _, str := range Strings {
		e := errors.New(str)
		err := Add(e)
		if werr, ok := err.(*WebError); ok {
			if werr.err != e || werr.message != "" || werr.statusCode != statusInternal {
				t.Errorf("Fail: got %v, %v, %v; want %v, %v, %v", werr.err, werr.message, werr.statusCode, e, "\"\"", statusInternal)
			}
			if werr.Error() != e.Error() {
				t.Errorf("Fail: got %v, want %v", werr.Error(), e.Error())
			}
		}
	}
}

func TestAddWithMessage(t *testing.T) {
	table := []struct {
		err          string
		msg          string
		expected_msg string
		expected_err string
	}{
		{"", "", "", ""},
		{"", "dcsdcds", "dcsdcds", "dcsdcds"},
		{"sdfre", "", "", "sdfre"},
		{"sssre", "sssdsc", "sssdsc", "sssdsc"},
	}
	for _, val := range table {
		e := errors.New(val.err)
		err := AddWithMessage(e, val.msg)
		if werr, ok := err.(*WebError); ok {
			if werr.err != e || werr.message != val.expected_msg || werr.statusCode != statusInternal {
				t.Errorf("Fail: got %v, %v, %v; want %v, %v, %v", werr.err, werr.message, werr.statusCode, e, val.expected_msg, statusInternal)
			}
			if werr.Error() != val.expected_err {
				t.Errorf("Fail: got %v, want %v", werr.Error(), val.expected_err)
			}
		}
	}
}

func TestAddHTTPError(t *testing.T) {
	table := []struct {
		err                 string
		msg                 string
		statusCode          int
		expected_err        string
		expected_msg        string
		expected_statusCode int
	}{
		{"", "", 0, "", "", 0},
		{"", "dcsdcds", 500, "dcsdcds", "dcsdcds", 500},
		{"sdfre", "", 123456789, "sdfre", "", 123456789},
		{"sssre", "sssdsc", -5, "sssdsc", "sssdsc", -5},
	}
	for _, val := range table {
		e := errors.New(val.err)
		err := AddHTTPError(e, val.msg, val.statusCode)
		if werr, ok := err.(*WebError); ok {
			if werr.err != e || werr.message != val.expected_msg || werr.statusCode != val.expected_statusCode {
				t.Errorf("Fail: got %v, %v, %v; want %v, %v, %v", werr.err, werr.message, werr.statusCode, e, val.expected_msg, val.expected_statusCode)
			}
			if werr.Error() != val.expected_err {
				t.Errorf("Fail: got %v, want %v", werr.Error(), val.expected_err)
			}
		}
	}
}

func TestLog(t *testing.T) {
	tables := []struct {
		in     string
		expect string
	}{
		{"Hello", "Hello"},
	}
	wr := writer("DDD")
	out = Writer(&wr)

	for _, table := range tables {
		if Log(errors.New(table.in)); string(*out.(*writer)) != table.expect {
			t.Errorf("FAIL: want \"%s\", got \"%s\"", table.expect, string(*out.(*writer)))
		}
	}
}
