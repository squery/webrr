package webrr

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"runtime"
	"time"
)

var (
	out      io.Writer = os.Stderr
	logIndex int       = 3
	prefix   string    = ""
)

const (
	statusInternal       = http.StatusInternalServerError
	statusForbidden      = http.StatusForbidden
	statusBadRequest     = http.StatusBadRequest
	statusNotImplemented = http.StatusNotImplemented
	defaultStatusCode    = statusInternal
)

type WebError struct {
	err        error
	message    string
	statusCode int
}

func (w *WebError) Error() string {
	if w.message != "" {
		return w.message
	} else {
		return w.err.Error()
	}
}

func (w *WebError) Read(p []byte) (int, error) {
	p, err := json.Marshal(&map[string]string{
		"time":         time.Now().Format("02-01-2006 15:04:05 MST"),
		"error":        http.StatusText(w.statusCode),
		"errorMessage": w.message,
	})
	return len(p), err
}

func Handler(f func(error)) {
	if r := recover(); r != nil {
		if err, ok := r.(error); ok {
			f(err)
		} else {
			panic(r)
		}
	}
}

func HandlerWithLog(f func(error)) {
	if r := recover(); r != nil {
		if err, ok := r.(error); ok {
			f(err)
			Log(err)
		} else {
			panic(r)
		}
	}
}

func GetWebError(err error) *WebError {
	if werr, ok := err.(*WebError); ok {
		return werr
	} else {
		return &WebError{
			err:        err,
			message:    "",
			statusCode: defaultStatusCode,
		}
	}
}

func CheckError(err error, stringErr string) bool {
	if werr, ok := err.(*WebError); ok {
		if werr.err.Error() == stringErr {
			return true
		} else {
			return false
		}
	} else {
		if err.Error() == stringErr {
			return true
		} else {
			return false
		}
	}
}

func CheckStatusCode(err error, statusCode int) bool {
	if werr, ok := err.(*WebError); ok {
		if werr.statusCode == statusCode {
			return true
		} else {
			return false
		}
	} else {
		if statusCode == statusInternal {
			return true
		} else {
			return false
		}
	}
}

func (w *WebError) Wrap(err error) error {
	if err == nil {
		return nil
	}
	if w.err != nil {
		w.err = fmt.Errorf("%s: %w", w.err.Error(), err)
		return w
	}
	w.err = err
	return w
}

func New(desc string, statusCode int) *WebError {
	err := errors.New(desc)
	if statusCode == 0 {
		statusCode = defaultStatusCode
	}
	return &WebError{
		err:        err,
		message:    desc,
		statusCode: statusCode,
	}
}

func NewErr(desc, msg string, statusCode int) error {
	err := errors.New(desc)
	if statusCode == 0 {
		statusCode = defaultStatusCode
	}
	return &WebError{
		err:        err,
		message:    msg,
		statusCode: statusCode,
	}
}

func NewRawErr(desc string) error {
	return errors.New(desc)
}

func IsForbidden(err error) bool {
	if werr, ok := err.(*WebError); ok {
		return werr.statusCode == statusForbidden
	}
	return false
}

func IsBadRequest(err error) bool {
	if werr, ok := err.(*WebError); ok {
		return werr.statusCode == statusBadRequest
	}
	return false
}

func AddWithMessage(err error, msg string) error {
	if werr, ok := err.(*WebError); ok {
		return werr
	}
	return &WebError{
		err:        err,
		message:    msg,
		statusCode: defaultStatusCode,
	}
}

func AddHTTPError(err error, msg string, statusCode int) error {
	if werr, ok := err.(*WebError); ok {
		return werr
	}
	return &WebError{
		err:        err,
		message:    msg,
		statusCode: statusCode,
	}
}

func Add(err error) error {
	if werr, ok := err.(*WebError); ok {
		return werr
	}
	return &WebError{
		err:        err,
		message:    "",
		statusCode: defaultStatusCode,
	}
}

func CheckWithMessage(err error, msg string) {
	if err != nil {
		panic(AddWithMessage(err, msg))
	}
}

func CheckHTTP(err error, msg string, statusCode int) {
	if err != nil {
		panic(AddHTTPError(err, msg, statusCode))
	}
}

func Check(err error) {
	if err != nil {
		panic(Add(err))
	}
}

func HTTPError(w http.ResponseWriter, err error) {
	var statusCode int
	var errorMessage string
	if werr, ok := err.(*WebError); ok {
		statusCode = werr.statusCode
		errorMessage = werr.message
	} else {
		statusCode = defaultStatusCode
		errorMessage = ""
	}

	msgMap := map[string]string{
		"time":         time.Now().Format("02-01-2006 15:04:05 MST"),
		"error":        http.StatusText(statusCode),
		"errorMessage": errorMessage,
	}
	bts, err := json.Marshal(&msgMap)
	if err != nil {
		panic(err.Error())
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(bts)
}

func Log(err error) {
	output(out, writeError(err, logIndex))
}

func LogWithMessage(err error) {
	output(out, writeErrorWithMessage(err, logIndex))
}

func LogWithIndex(err error, index int) {
	output(out, writeError(err, index))
}

func SetOutput(w io.Writer) {
	out = w
}

func SetLogIndex(index int) {
	logIndex = index
}

func SetLogPrefix(p string) {
	prefix = p
}

func LogWithWriter(w io.Writer, err error) {
	output(w, writeError(err, logIndex))
}

func RawError(err error) error {
	if werr, ok := err.(*WebError); ok {
		return werr.err
	}
	return err
}

func writeError(err error, index int) string {
	stringErr := ""
	if werr, ok := err.(*WebError); ok {
		stringErr = werr.err.Error()
	} else {
		stringErr = err.Error()
	}

	str := ""
	if err != nil {
		_, filename, line, ok := runtime.Caller(index)
		if !ok {
			filename = "???"
			line = 0
		}
		filename = shortFilename(filename)
		str = fmt.Sprintf("%s%s %s:%v %v%v", prefix, time.Now().Format("2006/01/02 15:04:05"), filename, line, stringErr, "\n")
	}
	return str
}

func writeErrorWithMessage(err error, index int) string {
	stringErr := ""
	message := ""
	if werr, ok := err.(*WebError); ok {
		stringErr = werr.err.Error()
		message = werr.message
	} else {
		stringErr = err.Error()
	}

	str := ""
	if err != nil {
		_, filename, line, ok := runtime.Caller(index)
		if !ok {
			filename = "???"
			line = 0
		}
		filename = shortFilename(filename)
		str = fmt.Sprintf("%s%s %s:%v %v %v\n", prefix, time.Now().Format("2006/01/02 15:04:05"), filename, line, stringErr, message)
	}
	return str
}

func shortFilename(filename string) string {
	short := filename
	for i := len(filename) - 1; i >= 0; i-- {
		if filename[i] == '/' {
			short = filename[i+1:]
			break
		}
	}
	return short
}

func output(w io.Writer, str string) {
	_, err := w.Write([]byte(str))
	if err != nil {
		panic(err.Error())
	}
}

func toJSON(str string) string {
	return fmt.Sprintf(`{"error":"%s"`, str)
}
